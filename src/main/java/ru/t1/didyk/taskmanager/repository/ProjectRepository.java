package ru.t1.didyk.taskmanager.repository;

import ru.t1.didyk.taskmanager.api.IProjectRepository;
import ru.t1.didyk.taskmanager.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

}
