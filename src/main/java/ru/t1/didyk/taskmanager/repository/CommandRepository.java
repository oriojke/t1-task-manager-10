package ru.t1.didyk.taskmanager.repository;

import ru.t1.didyk.taskmanager.api.ICommandRepository;
import ru.t1.didyk.taskmanager.constant.ArgumentConst;
import ru.t1.didyk.taskmanager.constant.CommandConst;
import ru.t1.didyk.taskmanager.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            CommandConst.INFO, ArgumentConst.INFO,
            "Show system info."
    );

    private static final Command ABOUT = new Command(
            CommandConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info."
    );

    private static final Command HELP = new Command(
            CommandConst.HELP, ArgumentConst.HELP,
            "Show command list."
    );

    private static final Command EXIT = new Command(
            CommandConst.EXIT, null,
            "Close application."
    );

    private static final Command VERSION = new Command(
            CommandConst.VERSION, ArgumentConst.VERSION,
            "Show application version."
    );

    private static final Command PROJECT_CLEAR = new Command(
            CommandConst.PROJECT_CLEAR, null,
            "Remove all projects."
    );

    private static final Command PROJECT_LIST = new Command(
            CommandConst.PROJECT_LIST, null,
            "Display all projects."
    );

    private static final Command PROJECT_CREATE = new Command(
            CommandConst.PROJECT_CREATE, null,
            "Create new project."
    );

    private static final Command TASK_CLEAR = new Command(
            CommandConst.TASK_CLEAR, null,
            "Remove all tasks."
    );

    private static final Command TASK_LIST = new Command(
            CommandConst.TASK_LIST, null,
            "Display all tasks."
    );

    private static final Command TASK_CREATE = new Command(
            CommandConst.TASK_CREATE, null,
            "Create new task."
    );

    private static final Command[] COMMANDS = new Command[]{
            INFO, ABOUT, HELP, EXIT, VERSION,
            PROJECT_CLEAR, PROJECT_LIST, PROJECT_CREATE,
            TASK_CLEAR, TASK_LIST, TASK_CREATE
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
