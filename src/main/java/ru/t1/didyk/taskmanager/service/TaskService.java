package ru.t1.didyk.taskmanager.service;

import ru.t1.didyk.taskmanager.api.ITaskRepository;
import ru.t1.didyk.taskmanager.api.ITaskService;
import ru.t1.didyk.taskmanager.model.Task;

import java.util.List;

public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task create(String name, String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(task);
        return task;
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public void add(Task task) {
        if (task == null) return;
        taskRepository.add(task);
    }
}
