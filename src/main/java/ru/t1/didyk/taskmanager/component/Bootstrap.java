package ru.t1.didyk.taskmanager.component;

import ru.t1.didyk.taskmanager.api.*;
import ru.t1.didyk.taskmanager.constant.ArgumentConst;
import ru.t1.didyk.taskmanager.constant.CommandConst;
import ru.t1.didyk.taskmanager.controller.CommandController;
import ru.t1.didyk.taskmanager.controller.ProjectController;
import ru.t1.didyk.taskmanager.controller.TaskController;
import ru.t1.didyk.taskmanager.repository.CommandRepository;
import ru.t1.didyk.taskmanager.repository.ProjectRepository;
import ru.t1.didyk.taskmanager.repository.TaskRepository;
import ru.t1.didyk.taskmanager.service.CommandService;
import ru.t1.didyk.taskmanager.service.ProjectService;
import ru.t1.didyk.taskmanager.service.TaskService;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);


    public void run(String[] args) {
        processArguments(args);
        processCommands();
    }

    private void processCommands() {
        System.out.println("** WELCOME TO TASK MANAGER **");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private void processArguments(String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
    }

    private void processCommand(String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.INFO:
                commandController.showSystemInfo();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.EXIT:
                commandController.showExit();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProject();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTask();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            default:
                commandController.showErrorCommand();
                break;
        }
    }

    private void processArgument(String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            default:
                commandController.showErrorArgument();
                break;
        }
        System.exit(0);
    }

}
