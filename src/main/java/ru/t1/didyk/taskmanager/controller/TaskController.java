package ru.t1.didyk.taskmanager.controller;

import ru.t1.didyk.taskmanager.api.ITaskController;
import ru.t1.didyk.taskmanager.api.ITaskService;
import ru.t1.didyk.taskmanager.model.Task;
import ru.t1.didyk.taskmanager.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[SHOW TASKS]");
        int index = 1;
        final List<Task> tasks = taskService.findAll();
        for (final Task task : tasks) {
            System.out.println(index + "." + task.getName());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if (task == null) System.out.println("[FAIL]");
        System.out.println("[OK]");
    }

}
