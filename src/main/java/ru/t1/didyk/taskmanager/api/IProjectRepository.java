package ru.t1.didyk.taskmanager.api;

import ru.t1.didyk.taskmanager.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    void clear();

    Project add(Project project);

}
