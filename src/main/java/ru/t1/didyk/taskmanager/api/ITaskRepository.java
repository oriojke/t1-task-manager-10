package ru.t1.didyk.taskmanager.api;

import ru.t1.didyk.taskmanager.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    void clear();

    Task add(Task task);

}
