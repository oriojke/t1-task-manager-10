package ru.t1.didyk.taskmanager.api;

import ru.t1.didyk.taskmanager.model.Project;

import java.util.List;

public interface IProjectService {

    Project create(String name, String description);

    List<Project> findAll();

    void clear();

    Project add(Project project);

}
