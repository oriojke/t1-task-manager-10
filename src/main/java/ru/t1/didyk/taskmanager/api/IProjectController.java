package ru.t1.didyk.taskmanager.api;

public interface IProjectController {

    void showProjects();

    void clearProjects();

    void createProject();

}
