package ru.t1.didyk.taskmanager.api;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();

}
