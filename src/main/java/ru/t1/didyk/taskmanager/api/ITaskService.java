package ru.t1.didyk.taskmanager.api;

import ru.t1.didyk.taskmanager.model.Task;

import java.util.List;

public interface ITaskService {

    Task create(String name, String description);

    List<Task> findAll();

    void clear();

    void add(Task task);

}
